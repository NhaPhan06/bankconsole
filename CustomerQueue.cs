﻿namespace ConsoleApp1;

public class CustomerQueue : CustomerQueueBase
{
    public CustomerQueue(int maxQueue) : base(maxQueue)
    {
    }


    public override void AddCustomerTop(Customer customer)
    {
        Customer[] temp = new Customer[customers.Length + 1];
        temp[0] = customer;
        int locate = 1;
        foreach (var VARIABLE in customers)
        {
            temp[locate] = VARIABLE;
            locate++;
        }

        customers = temp;

        foreach (var list in customers)
            if (list._Num >= 0)
                list._Num = list._Num + 1;
    }

    public override void AddCustomerBot(Customer customer)
    {
        Customer[] temp = new Customer[customers.Length + 1];
        int locate = 0;
        foreach (var VARIABLE in customers)
        {
            temp[locate] = VARIABLE;
            locate++;
        }

        temp[temp.Length - 1] = customer;
        customers = temp;
    }

    public override void GetAllCustomer()
    {
        foreach (var customer in customers)
        {
            Console.WriteLine("Num:" + customer._Num + " Name: " + customer._Name + ", Amount: " + customer._Amount);
        }
    }

    public override Customer GetCustomer(int i)
    {
        return customers[i];
    }

    public override int Length()
    {
        return customers.Length;
    }

    public override void RemoveCustomer(int index)
    {
        Customer[] temp = new Customer[customers.Length - 1];
        int locate = 0;
        for(int i = 1; i <= customers.Length - 1; i++)
        {
            temp[locate] = customers[i];
            locate++;
        } 
        customers = temp;
        foreach (var list in customers)
            if (list._Num >= 0)
                list._Num = list._Num - 1;
    }
}