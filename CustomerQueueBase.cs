﻿namespace ConsoleApp1;

public abstract class CustomerQueueBase
{
    protected Customer[] customers;

    public CustomerQueueBase(int maxQueue)
    {
        customers = new Customer[maxQueue];
    }
    public abstract void AddCustomerTop(Customer customer);
    public abstract void AddCustomerBot(Customer customer);
    public abstract void GetAllCustomer();
    public abstract Customer GetCustomer(int i);
    public abstract int Length();
    public abstract void RemoveCustomer(int index);
}