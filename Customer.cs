﻿namespace ConsoleApp1;

public class Customer
{
    public Customer(int num, string name, decimal amount)
    {
        _Num = num;
        _Name = name;
        _Amount = amount;
    }
    
    public int _Num { get; set; }
    public string _Name {get; set; }
    public decimal _Amount { get; set; }
}