﻿namespace ConsoleApp1;

public class Function
{
    private CustomerQueue queue;
    private CustomerQueue done;
    string filePath = "Customer.txt";

    public Function(CustomerQueue queue, CustomerQueue done)
    {
        this.queue = queue;
        this.done = done;
    }
    

    public void CheckCustomer()
    {
        Console.WriteLine("Input Name: ");
        var name = Console.ReadLine();
        Console.WriteLine("Input Withdraw: ");
        var amount = InputDecimal();

        var customer = new Customer(queue.Length() + 1, name, amount);

        if (amount >= 1000000)
            queue.AddCustomerTop(customer);
        else
            queue.AddCustomerBot(customer);
    }

    public void WithdrawMoney()
    {
        if (queue.Length() == 0)
        {
            Console.WriteLine("Queue is empty.");
        }
        else
        {
            var customer = queue.GetCustomer(0);
            done.AddCustomerBot(customer);
            queue.RemoveCustomer(0);


            Console.WriteLine($"Num: {customer._Num}, Name: {customer._Name}, Amount: {customer._Amount}");
        }
    }

    //3
    public void ShowInfor()
    {
        queue.GetAllCustomer();
        Console.WriteLine($"Total number of customers waiting is: {queue.Length()}");
    }

    //4
    public void Statistical()
    {
        decimal totalQueue = 0;
        for (int i = 0; i < queue.Length(); i++)
        {
            totalQueue += queue.GetCustomer(i)._Amount;
        }

        decimal totalDone = 0;
        for (int i = 0; i < done.Length(); i++)
        {
            totalDone += done.GetCustomer(i)._Amount;
        }

        Console.WriteLine("Customers who have withdrawn: ");
        done.GetAllCustomer();

        Console.WriteLine("Total amount withdrawn: " + totalDone);
        Console.WriteLine("Total amount remaining: " + totalQueue);
    }


    //6
    public void WriteToFile()
    {
        if (!File.Exists(filePath))
        {
            File.CreateText(filePath);
        }

        using (var sw = new StreamWriter(filePath))
        {
            for (int i = 0; i < queue.Length(); i++)
            {
                Customer customer = queue.GetCustomer(i);
                sw.WriteLine($"{customer._Name};{customer._Amount}");
            }
        }
    }

    public void ReadToFile()
    {
        string filePath = "Customer.txt";
        using (var sw = new StreamReader(filePath))
        {
            string line;
            var num = 1;
            while ((line = sw.ReadLine()) != null)
            {
                var txt = line.Split(";");
                var name = txt[0];
                var amount = txt[1];
                Customer customer = new Customer(num, name, Decimal.Parse(amount));
                queue.AddCustomerBot(customer);
                num++;
            }

            sw.Close();
        }
    }


    public decimal InputDecimal()
    {
        decimal input = 0;
        while (true)
        {
            try
            {
                input = Decimal.Parse(Console.ReadLine());
                break;
            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid! Please input again: ");
            }
        }

        return input;
    }
}