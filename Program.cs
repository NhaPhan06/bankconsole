﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var program = new Program();
            program.Run();
        }

        private Function function;
        private Menu menu;

        public Program()
        {
            var queue = new CustomerQueue(0);
            var done = new CustomerQueue(0);
            function = new Function(queue, done);
            menu = new Menu();
        }

        public void Run()
        {
            try
            {
                function.ReadToFile();
            }
            catch (Exception e)
            {

            }
            finally
            {
                int choice = 0;

                do
                {
                    menu.DisplayMenu();
                    Console.WriteLine("Enter your choice: ");
                    choice = Int32.Parse(Console.ReadLine());

                    switch (choice)
                    {
                        case 1:
                            function.CheckCustomer();
                            break;
                        case 2:
                            function.WithdrawMoney();
                            break;
                        case 3:
                            function.ShowInfor();
                            break;
                        case 4:
                            function.Statistical();
                            break;
                        case 5:
                            function.WriteToFile();
                            break;
                    }
                } while (choice != 5);
            }
        }
    }
}